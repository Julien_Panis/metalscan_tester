#pragma config FOSC = INTRCIO
#define _XTAL_FREQ 4000000

#include <xc.h>

/*
 GP0 [o] = ICSDAT (vibration pattern)
 GP1 [i] = ICSCLK (analog comparator level)
 GP2 [o] = PWR_EN
 GP3 [i] = VPP
 GP4 [o] = PULSE
 GP5 [i] = BPON
 */

int main(void) {   
    // Set GPIO direction
    TRISIO = 0x2A; //< 0b00101010
    
    // ICSDAT = 0 / PWR_EN = 0 / PULSE = 0
    GPIObits.GP0 = 0;
    GPIObits.GP2 = 0;
    GPIObits.GP4 = 0;
    
    // Set analog comparator
    CMCONbits.CINV = 1; //< Comparator Output Inversion bit
    CMCONbits.CIS  = 0; //< Comparator Input Switch bit
    CMCONbits.CM   = 4; //< Comparator w/o Output and with Internal Reference
    
    VRCONbits.VRR  = 1; //< CVREF Range Selection bit = Low range
    VRCONbits.VR   = 2; //< When VRR = 1: CVREF = (VR3:VR0 / 24) * VDD   --->   1 unit = 208 mV [@ VDD = 5V]
    VRCONbits.VREN = 1; //< CVREF Enable bit
    
    // PWR_EN = 1
    GPIObits.GP2 = 1;
    __delay_ms(25);
    
    while (1) {
        // Check comparator output
        if (CMCONbits.COUT == 0) {
            // Continuous vibration
            GPIObits.GP0 = 1;
        } else {
            // Discontinuous vibration
            GPIObits.GP0 = 1;
            __delay_ms(200);
            GPIObits.GP0 = 0;
            __delay_ms(200);
        }
    }
    
    return 0;
}
